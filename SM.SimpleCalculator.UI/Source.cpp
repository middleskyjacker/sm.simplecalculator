#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2) {
	return num1 + num2;
}

float Subtract(float num1, float num2) {
	return num1 - num2;
}

float Multiply(float num1, float num2) {
	return num1 * num2;
}

float Divide(float num1, float num2) {
	return num1 / num2;
}

void Repeat(string r, bool repeat) {
	cout << "Do you want to continue? (Y = Yes, Any other character = No.): ";
	cin >> r;
	if (r != "Y" and r != "y") repeat = false;
}

int main() {

	float x;
	float y;
	string e;
	bool repeat = true;
	string r;
	cout << "Welcome to the Simple Calculator. When entering numbers, make sure they are positive.\n";
	
	while (repeat == true) {
		cout << "Please enter your first number: ";
		cin >> x;
		cout << "Please enter your second number: ";
		cin >> y;
		cout << "You have entered " << x << " and " << y << ". What do you want to do with these numbers?\n";
		cout << "Select a symbol from this list(+, -, *, /): ";
		cin >> e;
		if (e == "+") {
			cout << "Your answer to " << x << " " << e << " " << y << " = " << Add(x, y) << endl;
			Repeat(r, repeat);
		}
		else if (e == "-") {
			cout << "Your answer to " << x << " " << e << " " << y << " = " << Subtract(x, y) << endl;
			Repeat(r, repeat);
		}
		else if (e == "*") {
			cout << "Your answer to " << x << " " << e << " " << y << " = " << Multiply(x, y) << endl;
			Repeat(r, repeat);
		}
		else if (e == "/") {
			cout << "Your answer to " << x << " " << e << " " << y << " = " << Divide(x, y) << endl;
			Repeat(r, repeat);
		}
		else {
			cout << "Invalid alert! Yay, my first time snarking on a project. Next time, listen to the instructions. ";
			repeat = false;
		}
	}
	cout << "Goodbye!";

	(void)_getch();
	return 0;
}